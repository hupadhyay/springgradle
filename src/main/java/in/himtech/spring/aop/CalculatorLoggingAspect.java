package in.himtech.spring.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class CalculatorLoggingAspect {

	@Before("execution(* ArithmeticCalculator.add(..))")
	public void logBefore() {
		System.out.println("this is Simple before advice.");
	}

	@Before("execution(* *.*(..))")
	public void logBefore(JoinPoint joinPoint) {
		System.out.println("Before Advice: Method Name: "
				+ joinPoint.getSignature().getName() + ", Arguments: "
				+ joinPoint.getArgs());
	}
	
	@After("execution(* *.*(..))")
	public void logAfter(JoinPoint joinPoint) {
		System.out.println("After Advice: Method Name: "
				+ joinPoint.getSignature().getName() + " finished.");
	}

}
