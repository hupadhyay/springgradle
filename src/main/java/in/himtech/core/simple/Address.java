package in.himtech.core.simple;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Qualifier(value="add")
public class Address {
	private int houseNumber;
	private String locality;
	private String district;

	public int getHouseNumber() {
		return houseNumber;
	}

	@Value("#{T(java.lang.Math).random()*100}")
	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getLocality() {
		return locality;
	}

	@Value("Sarojini Nagar")
	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getDistrict() {
		return district;
	}

	@Value("Lucknow")
	public void setDistrict(String district) {
		this.district = district;
	}

	@Override
	public String toString() {
		return "Address [houseNumber=" + houseNumber + ", locality=" + locality
				+ ", district=" + district + "]";
	}

}
