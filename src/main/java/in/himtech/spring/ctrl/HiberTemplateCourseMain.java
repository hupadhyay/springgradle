package in.himtech.spring.ctrl;

import java.util.Calendar;
import java.util.List;

import in.himtech.spring.dao.CourseDao;
import in.himtech.spring.dto.Course;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HiberTemplateCourseMain {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"beans-hibernate-template.xml");

		CourseDao courseDao = context.getBean(CourseDao.class);

		 storeCourse(courseDao);
		 
//		List<Course> listCourse = courseDao.findAll();
//
//		for (Course course : listCourse) {
//
//			System.out.println(course.getTitle());
//		}

//		courseDao.delete(12);
		
		((ConfigurableApplicationContext) context).close();
	}

	private static void storeCourse(CourseDao courseDao) {
		Course course = new Course();
		course.setEndDate(Calendar.getInstance().getTime());
		course.setStartDate(Calendar.getInstance().getTime());
		course.setTitle("Hritika Book Service");
		course.setFee(100.00);

		courseDao.store(course);

		course = new Course();
		course.setEndDate(Calendar.getInstance().getTime());
		course.setStartDate(Calendar.getInstance().getTime());
		course.setTitle("Hrishik Book Service");
		course.setFee(200.00);

		courseDao.store(course);

		course = new Course();
		course.setEndDate(Calendar.getInstance().getTime());
		course.setStartDate(Calendar.getInstance().getTime());
		course.setTitle("CSC Induction Book");
		course.setFee(300.25);

		courseDao.store(course);
	}

}
