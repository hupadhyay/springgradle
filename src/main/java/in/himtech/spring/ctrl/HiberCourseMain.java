package in.himtech.spring.ctrl;

import java.util.Calendar;
import java.util.List;

import in.himtech.spring.dao.CourseDao;
import in.himtech.spring.dto.Course;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HiberCourseMain {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"beans-hibernate.xml");

		CourseDao courseDao = context.getBean(CourseDao.class);

		// storeCourse(courseDao);
		List<Course> listCourse = courseDao.findAll();

		for (Course course : listCourse) {

			System.out.println(course.getTitle());
		}

		((ConfigurableApplicationContext) context).close();
	}

	private static void storeCourse(CourseDao courseDao) {
		Course course = new Course();
		course.setEndDate(Calendar.getInstance().getTime());
		course.setStartDate(Calendar.getInstance().getTime());
		course.setTitle("Hibernate Tutoiral");
		course.setFee(345.23);

		courseDao.store(course);

		course = new Course();
		course.setEndDate(Calendar.getInstance().getTime());
		course.setStartDate(Calendar.getInstance().getTime());
		course.setTitle("JPA Tutoiral");
		course.setFee(4847.23);

		courseDao.store(course);

		course = new Course();
		course.setEndDate(Calendar.getInstance().getTime());
		course.setStartDate(Calendar.getInstance().getTime());
		course.setTitle("Spring DAO Tutoiral");
		course.setFee(7576.23);

		courseDao.store(course);
	}

}
