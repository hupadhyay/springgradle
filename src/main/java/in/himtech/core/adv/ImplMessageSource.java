package in.himtech.core.adv;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

public class ImplMessageSource implements MessageSourceAware {

	private MessageSource messageSource;

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getMessage(String strKey, Object[]obj){
		
		return messageSource.getMessage(strKey, obj, Locale.getDefault());
	}
}
