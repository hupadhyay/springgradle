package in.himtech.spring.dao;

import in.himtech.spring.dto.Account;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

@Repository
public class AccountDaoImpl implements AccountDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public String createAccount(Account account) {
		String str = "insert into account(account_no, name, balance) values (?,?,?)";
		jdbcTemplate.update(str, account.getAccountNumber(), account.getName(),
				account.getBalance());
		return "Data inserted Successfully";
	}

	@Override
	public boolean deleteAccount(Account account) {
		String str = "delete from account where account_no = ?";
		try{
			jdbcTemplate.update(str, account.getAccountNumber());
			return true;
		} catch(DataAccessException dae) {
			System.out.println(dae.getMessage());
			return false;
		}
		
	}

	@Override
	public Account findAccount(String prmAccntNum) {
		String str = "select name, balance from account where account_no=?";
		Account account = jdbcTemplate.query(str, new Object[] { prmAccntNum },
				new AccountResultSetExtractor());
		account.setAccountNumber(prmAccntNum);
		return account;
	}

	@Override
	public String updateAccount(Account account) {
		String str = "update account set name=?, balance=? where account_no=?";
		jdbcTemplate.update(str, new Object[] { account.getName(),
				account.getBalance(), account.getAccountNumber() });
		return "Data updated successfully";
	}

	@Override
	public List<Account> fetchAllAccounts() {
		String str = "select account_no, name, balance from account";
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(str);
		
		List<Account> listAccount = new ArrayList<Account>();
		
		for(Map<String, Object> rowMap : rows){
			Account account = new Account();
			account.setAccountNumber((String)rowMap.get("account_no"));
			account.setName((String)rowMap.get("name"));
			account.setBalance((Double)rowMap.get("balance"));
			listAccount.add(account);
		}
		return listAccount;
	}

	@Override
	public Double getSumOfBalance() {
		String sql = "select sum(balance) from account";
		double totalBalance = jdbcTemplate.queryForObject(sql, Double.class);
		return totalBalance;
	}

	@Override
	public int getTotalNumberOfAccounts() {
		String sql = "select count(account_no) from account";
		int totalBalance = jdbcTemplate.queryForObject(sql, Integer.class);
		return totalBalance;
	}

	@Override
	public String getAccountHolderName(String prmAccntNum) {
		String sql = "select name from account where account_no=?";
		String name = jdbcTemplate.queryForObject(sql, String.class, prmAccntNum);
		return name;
	}

	@Override
	public Double getBalance(String prmAccntNum) {
		String sql = "select balance from account where account_no=?";
		double balance = jdbcTemplate.queryForObject(sql, Double.class, prmAccntNum);
		return balance;
	}
}

class AccountResultSetExtractor implements ResultSetExtractor<Account> {

	@Override
	public Account extractData(ResultSet rs) throws SQLException,
			DataAccessException {
		Account account = new Account();
		rs.next();
		account.setBalance(rs.getDouble(2));
		account.setName(rs.getString(1));
		return account;
	}
}
