package in.himtech.spring.dao;

import in.himtech.spring.dto.Course;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class JpaContextDaoImpl implements CourseDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional
	public void store(Course course) {
		entityManager.merge(course);
	}

	@Transactional
	public void delete(Integer courseId) {
		Course course = entityManager.find(Course.class, courseId);
		entityManager.remove(course);
	}

	@Transactional(readOnly=true)
	public Course findById(Integer courseId) {
		Course course = entityManager.find(Course.class, courseId);
		return course;
	}

	@Transactional(readOnly=true)
	public List<Course> findAll() {
		Query query =entityManager.createQuery("from Course");
		return query.getResultList();
	}

}
