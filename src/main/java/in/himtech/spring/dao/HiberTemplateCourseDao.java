package in.himtech.spring.dao;

import in.himtech.spring.dto.Course;

import java.util.List;

import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class HiberTemplateCourseDao implements CourseDao {

	private HibernateTemplate hibernateTemplate;
	
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	
	@Transactional
	public void store(Course course) {
		hibernateTemplate.saveOrUpdate(course);
	}

	@Transactional
	public void delete(Integer courseId) {
		Course course = hibernateTemplate.get(Course.class, courseId);
		hibernateTemplate.delete(course);
	}

	@Transactional(readOnly=true)
	public Course findById(Integer courseId) {
		return hibernateTemplate.get(Course.class, courseId);
	}

	@Transactional(readOnly=true)
	public List<Course> findAll() {
		return (List<Course>)hibernateTemplate.find("from Course");
	}

}
