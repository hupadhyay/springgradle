package in.himtech.spring.dao;

import in.himtech.spring.dto.Course;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class HimbernateContextualCourseDao implements CourseDao {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public void store(Course course) {
		sessionFactory.getCurrentSession().saveOrUpdate(course);
	}

	@Transactional
	public void delete(Integer courseId) {
		Course course = (Course) sessionFactory.getCurrentSession().get(
				Course.class, courseId);
		sessionFactory.getCurrentSession().delete(course);
	}

	@Transactional(readOnly = true)
	public Course findById(Integer courseId) {
		return (Course) sessionFactory.getCurrentSession().get(Course.class,
				courseId);
	}

	@Transactional(readOnly = true)
	public List<Course> findAll() {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Course");

		return (List<Course>) query.list();
	}

}
