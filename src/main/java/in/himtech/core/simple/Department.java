package in.himtech.core.simple;

public class Department {
	private String name;
	private String deptId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	@Override
	public String toString() {
		return "Department [name=" + name + ", deptId=" + deptId + "]";
	}

}
