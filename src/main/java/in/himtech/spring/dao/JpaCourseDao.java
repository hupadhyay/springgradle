package in.himtech.spring.dao;

import in.himtech.spring.dto.Course;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

public class JpaCourseDao implements CourseDao {

	private EntityManagerFactory entityManagerFactory;
	
	public void setEntityManagerFactory(
			EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}
	
	@Override
	public void store(Course course) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction tx = entityManager.getTransaction();
		
		try{
			tx.begin();
			entityManager.merge(course);
			tx.commit();
		}catch(PersistenceException ex){
			System.out.println(ex.getMessage());
			throw ex;
		}finally{
			entityManager.close();
		}
	}

	@Override
	public void delete(Integer courseId) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction tx = entityManager.getTransaction();
		
		try{
			tx.begin();
			Course course = entityManager.find(Course.class,  courseId);
			entityManager.remove(course);
			tx.commit();
		}catch(PersistenceException ex){
			System.out.println(ex.getMessage());
			throw ex;
		}finally{
			entityManager.close();
		}

	}

	@Override
	public Course findById(Integer courseId) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Course course = entityManager.find(Course.class, courseId);
		return course;
	}

	@Override
	public List<Course> findAll() {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query query = entityManager.createQuery("select course from Course course");
		return query.getResultList();
	}

}
