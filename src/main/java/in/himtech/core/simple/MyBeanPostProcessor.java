package in.himtech.core.simple;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanPostProcessor implements BeanPostProcessor{

	public Object postProcessAfterInitialization(Object obj, String beanName)
			throws BeansException {
		System.out.println(beanName + ": Post Processor After initialization.");
		return obj;
	}

	public Object postProcessBeforeInitialization(Object obj, String beanName)
			throws BeansException {
		System.out.println(beanName + ": Post Processor Before Initialization.");
		return obj;
	}

}
