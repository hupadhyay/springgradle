package in.himtech.spring.dao;

import java.util.List;

import in.himtech.spring.dto.Account;

public interface AccountDao {
	
	String createAccount(Account account);

	String updateAccount(Account account);

	boolean deleteAccount(Account account);

	Account findAccount(String prmAccntNum);
	
	List<Account> fetchAllAccounts();
	
	Double getSumOfBalance();
	
	int getTotalNumberOfAccounts();
	
	String getAccountHolderName(String prmAccntNum);
	
	Double getBalance(String prmAccntNum);
	
}
