package in.himtech.spring.aop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applnContextAdv.xml");
		
		ArithmeticCalculator arithCalc = context.getBean("arithmeticCalculator", ArithmeticCalculator.class);
		double result = arithCalc.add(5, 4);
		System.out.println(result);

	}

}
