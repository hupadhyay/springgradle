package in.himtech.spring.ctrl;

import in.himtech.spring.dao.AccountDao;
import in.himtech.spring.dto.Account;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class AccountController {

	@Autowired
	private AccountDao accountDao;
	
	private AtomicLong counter = new AtomicLong(1);
	
	public List<Account> fetchAllAccounts(){
		return null;
	}
	
	public String createAccount(String name, Double amount){
		Account account = new Account();
		account.setAccountNumber(String.valueOf(counter.getAndIncrement()));
		account.setName(name);
		account.setBalance(amount);
		String message = accountDao.createAccount(account);
		return message;
	}
	
	public String updateAccount(Double amount, String id){
		Account account = accountDao.findAccount(id);
		account.setBalance(amount);
		String message = accountDao.updateAccount(account);
		return message;
	}
	
	public String deleteAccount(String id){
		Account account = accountDao.findAccount(id);
		boolean bool = accountDao.deleteAccount(account);
		if(bool){
			return "Account deleted successfully!";
		} else {
			return "Account can not be deleted!";
		}
	}
	
	public Account fetchAccount(String id){
		return accountDao.findAccount(id);
	}
	
	public String getSumOfBalance(){
		return String.valueOf(accountDao.getSumOfBalance());
	}
	
	public String getTotalNumberOfAccounts(){
		return String.valueOf(accountDao.getTotalNumberOfAccounts());
	}
	
	public String getAccountHolderName(String id){
		return accountDao.getAccountHolderName(id);
	}
	
	public String getBalance(String id){
		return String.valueOf(accountDao.getBalance(id));
	}
	
}
