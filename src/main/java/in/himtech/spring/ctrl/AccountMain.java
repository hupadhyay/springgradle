package in.himtech.spring.ctrl;

import in.himtech.spring.dto.Account;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AccountMain {
	public static void main(String[] args) {
		ApplicationContext applnContext = new ClassPathXmlApplicationContext("applnContextDao.xml");
		
		AccountController accntCntrl = applnContext.getBean(AccountController.class);
		
//		System.out.println(accntCntrl.createAccount("Lata", 15000.0d));
		
		Account accnt = null;
		
		for(int i=1; i < 5; i++){
			accnt = accntCntrl.fetchAccount(String.valueOf(i));
			System.out.println(i + ": " + accnt);
		}
		
		
		((ConfigurableApplicationContext)applnContext).close();
		
	}
}
