package in.himtech.core.simple;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class Person implements BeanNameAware{
	public static String nation = "India"; 
	private String name;
	private int age;
	private double salary;
	private Address address;
	private String idName;

	public String getName() {
		return name;
	}

	@Value("Himanshu")
	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	@Value("12")
	public void setAge(int age) {
		this.age = age;
	}

	public double getSalary() {
		return salary;
	}

	@Value("5000")
	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public String getIdName() {
		return idName;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", salary=" + salary
				+ "]";
	}

	public void setBeanName(String beanName) {
		this.idName = beanName;
	}

}
