package in.himtech.core.adv;

import java.util.Locale;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applnContextAdv.xml");
		
		Shape shape = context.getBean("shape", Shape.class);
		System.out.println(shape.getName());
		System.out.println(shape.getSides());
		System.out.println(shape.getMessage());
		
		// accessing messages from properties file using application context object
		System.out.println(context.getMessage("msg.greeting",null, Locale.getDefault()));
		System.out.println(context.getMessage("msg.hny.greeing",new Object[]{"2015"}, Locale.US));
		System.out.println(context.getMessage("msg.chunavi.greeing",new Object[]{"bahano", "bhaiyon", "pranaam"}, Locale.US));
		
		ImplMessageSource msgSource = context.getBean("msgbundle", ImplMessageSource.class);
		System.out.println(msgSource.getMessage("msg.greeting",null));
		System.out.println(msgSource.getMessage("msg.hny.greeing",new Object[]{"2015"}));
		System.out.println(msgSource.getMessage("msg.chunavi.greeing",new Object[]{"bahano", "bhaiyon", "pranaam"}));
		
		((ConfigurableApplicationContext)context).close();
	}
}
