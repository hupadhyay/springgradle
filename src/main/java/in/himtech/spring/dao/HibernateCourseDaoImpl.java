package in.himtech.spring.dao;

import in.himtech.spring.dto.Course;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class HibernateCourseDaoImpl implements CourseDao {

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public void store(Course course) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.getTransaction();
		tx.begin();
		try{
			session.saveOrUpdate(course);
			tx.commit();
		} catch(HibernateException ex){
			tx.rollback();
			System.out.println(ex.getMessage());
			throw ex;
		}finally{
			session.close();
		}
	}

	@Override
	public void delete(Integer courseId) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.getTransaction();
		tx.begin();
		try{
			Course course = (Course)session.get(Course.class, courseId);
			session.delete(course);
			tx.commit();
		} catch(HibernateException ex){
			tx.rollback();
			System.out.println(ex.getMessage());
			throw ex;
		}finally{
			session.close();
		}
	}

	@Override
	public Course findById(Integer courseId) {
		Session session = sessionFactory.openSession();
		Course course = (Course)session.get(Course.class, courseId);
		return course;
	}

	@Override
	public List<Course> findAll() {
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Course");
		List<Course> listOfCourse = query.list();
		return listOfCourse;
	}
}
