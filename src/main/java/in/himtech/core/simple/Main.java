package in.himtech.core.simple;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applnContext.xml");
		Person person = context.getBean("prsn", Person.class);
		System.out.println(person);
		System.out.println(person.getAddress());
		System.out.println("Bean Name: " + person.getIdName());
		
		//Accessing of private static field
		String nation = context.getBean("nation", String.class);
		System.out.println("Static field: " + nation);
		
		//Accessing of instance filed 
		Address add = context.getBean("prsnAdd", Address.class);
		System.out.println(add);
		
		//Scope Access test
		Employee emp1 = context.getBean("emp1", Employee.class);
		System.out.println(emp1);
		emp1.getDept().setDeptId("D002");
		emp1.setSalary(5000.0);
		
		Employee emp2 = context.getBean("emp1", Employee.class);
		System.out.println(emp2);
		
		Employee emp3 = context.getBean("emp2", Employee.class);
		System.out.println(emp3);
		
		MyBanner banner = context.getBean("banner", MyBanner.class);
		banner.showBanner();
		
		((ConfigurableApplicationContext)context).close();
	}
}
