package in.himtech.spring.dao;

import java.util.List;

import in.himtech.spring.dto.Course;

public interface CourseDao {

	void store(Course course);
	void delete(Integer courseId);
	Course findById(Integer courseId);
	List<Course> findAll();
}
