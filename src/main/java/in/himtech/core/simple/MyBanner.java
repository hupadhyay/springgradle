package in.himtech.core.simple;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.core.io.Resource;


public class MyBanner {
	private Resource banner;
	
	public Resource getBanner() {
		return banner;
	}
	
	public void setBanner(Resource banner) {
		this.banner = banner;
	}
	
	public void initialize(){
		System.out.println("This is my initialization method for Banner");
	}
	
	public void destroy(){
		System.out.println("This is my distruction method for Banner");
	}
	
	public void showBanner(){
		try {
			InputStream is = banner.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String str;
			while((str = br.readLine()) != null){
				System.out.println(str);
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
